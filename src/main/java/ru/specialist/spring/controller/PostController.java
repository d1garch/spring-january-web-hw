package ru.specialist.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.specialist.spring.entity.User;
import ru.specialist.spring.repository.PostRepository;
import ru.specialist.spring.repository.UserRepository;
import ru.specialist.spring.service.UserService;

@Controller
//@RequestMapping("/blog")  //изменил 21:28
public class PostController {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;

    @Autowired
    public PostController(PostRepository postRepository,
                          UserRepository userRepository,
                          UserService userService) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @GetMapping
    public String posts(@RequestParam(name = "q", required = false) String query,
                        ModelMap model){
        if(StringUtils.hasText(query)) {
            model.put("posts",
                    postRepository.findByContentContainingIgnoreCaseOrderByDtCreatedDesc(query));
            model.put("title", "Search by");
            model.put("subtitle", query.length() < 20 ? query : query.substring(0, 20) + "...");
        }
        else {
            model.put("posts", postRepository.findAll(Sort.by("dtCreated").descending()));
            model.put("title", "All posts");
        }
        setCommonParams(model);
        return "blog";
    }

    @GetMapping("/user/{username}")
    public String postsByUser(@PathVariable String username, ModelMap model){
        User user = userService.findByUsername(username);
        model.put("posts", user.getPosts());
        model.put("title", "Search by");
        model.put("subtitle", username);
        setCommonParams(model);
        return "blog";
    }

    private void setCommonParams(ModelMap model){
        model.put("users", userRepository.findAll());
    }
}
